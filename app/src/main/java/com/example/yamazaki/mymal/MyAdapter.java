package com.example.yamazaki.mymal;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Yamazaki on 22.01.2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private DataManipulator dm;
    View.OnClickListener clickListener;
    OnDeleteButtonClickListener deleteButtonListener;
    OnEditButtonClickListener editButtonListener;

    public MyAdapter() {

    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_note, viewGroup, false);
        return new ViewHolder(v);
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (clickListener != null) {
            viewHolder.itemView.setOnClickListener(clickListener);
        }
        if (deleteButtonListener != null) {
            ViewHolder inViewHolder = (ViewHolder) viewHolder;
            inViewHolder.getDeleteButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteButtonListener.onDeleteIsClick(v, position);
                }
            });
        }
        if (editButtonListener != null) {
            ViewHolder inViewHolder = (ViewHolder) viewHolder;
            inViewHolder.getEditButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editButtonListener.onEditIsClick(v, position);
                }
            });
        }
        ListItem record = dm.getInstance().getItem(position);
        viewHolder.name.setText(record.getTitle());
    }

    @Override
    public int getItemCount() {
        return dm.getInstance().getSize();
    }


    public void setOnItemClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setDeleteButtonListener(OnDeleteButtonClickListener deleteButtonListener) {
        this.deleteButtonListener = deleteButtonListener;
    }

    public void setEditButtonListener(OnEditButtonClickListener editButtonListener) {
        this.editButtonListener = editButtonListener;
    }


    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public AppCompatImageButton editButton;
        public AppCompatImageButton deleteButton;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.my_text_view);
            editButton = (AppCompatImageButton) itemView.findViewById(R.id.edit_button);

            deleteButton = (AppCompatImageButton) itemView.findViewById(R.id.delete_button);

        }

        public AppCompatImageButton getDeleteButton() {
            return deleteButton;
        }

        public AppCompatImageButton getEditButton() {
            return editButton;
        }


    }
}
