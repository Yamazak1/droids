package com.example.yamazaki.mymal;

/**
 * Created by Yamazaki on 25.01.2017.
 * template for notes
 */

public class ListItem {
    private static int idGen = 1;
    private int id;
    private String title;
    private String description;


    public ListItem(String title, String description) {
        this.id = idGen++;
        this.title = title;
        this.description = description;
    }

    public ListItem(String title) {
        this.id = idGen++;
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
