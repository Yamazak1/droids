package com.example.yamazaki.mymal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Yamazaki on 01.02.2017.
 */

public class EditActivity extends AppCompatActivity implements View.OnClickListener {
    private AppCompatEditText editTitle;
    private AppCompatEditText editDescription;
    private FloatingActionButton confirmButton;
    private int editedId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_edit);
        editTitle = (AppCompatEditText) findViewById(R.id.title);
        editDescription = (AppCompatEditText) findViewById(R.id.description);
        confirmButton = (FloatingActionButton) findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(this);
        Intent intent = getIntent();
        if (intent != null) {
            editedId = intent.getIntExtra("Id", -1);
            editTitle.setText(intent.getStringExtra("Title"));
            editDescription.setText(intent.getStringExtra("Description"));
            Log.v("EditActivity", "Got Id:" + editedId);
        }


    }

    public void onClick(View v) {
        String title = editTitle.getText().toString();
        String description = editDescription.getText().toString();
        if (TextUtils.isEmpty(title)) {
            editTitle.setError("Title can not be empty");
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Command", "UPDATE");
        intent.putExtra("Id", editedId);
        intent.putExtra("Title", title);
        intent.putExtra("Description", description);
        setResult(RESULT_OK, intent);
        finish();
    }

}