package com.example.yamazaki.mymal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;

import static java.lang.System.err;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton addButton;
    //private DataManipulator dm;
    private int dbg_firsttime = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        addButton = (FloatingActionButton) findViewById(R.id.add_button);
        addButton.setOnClickListener(this);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //FIXME: заполнение данными выдели в отдельный приватный метод - это улучшает структуру
        //кода. Также можно назначить адаптер с пустой коллекцией при инициализации RecyclerView,
        //а при инициализации данных обновлять их у адаптера через метод
        //MyAdapter.setData(final Collection<ListItem> items)
        SetData();
        adapter = new MyAdapter();

        adapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = recyclerView.getChildAdapterPosition(v);
                ListItem item = DataManipulator.getInstance().getItem(position);
                Log.v("CLICKED", "Clicking on item(" + position + ", " + item + ")");
            }
        });
        adapter.setDeleteButtonListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteIsClick(View button, int position) {
                ListItem item = DataManipulator.getInstance().getItem(position);
                Log.v("CLICKED", "Clicking on delete button of item(" + position + ", " + item + ")");
                try {
                    DataManipulator.getInstance().executeCommand("DELETE", item.getId(), null, null);
                } catch (InvalidCommandException e) {
                    err.print(e.getStackTrace());
                    return;
                }
                recyclerView.removeViewAt(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, DataManipulator.getInstance().getSize());

            }
        });
        adapter.setEditButtonListener(new OnEditButtonClickListener() {
            @Override
            public void onEditIsClick(View button, int position) {
                ListItem item = DataManipulator.getInstance().getItem(position);
                Log.v("CLICKED", "Clicking on edit button of item(" + position + ", " + item + ")");
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                intent.putExtra("Id", item.getId());
                intent.putExtra("Title", item.getTitle());
                intent.putExtra("Description", item.getDescription());
                startActivityForResult(intent, 2);
            }
        });
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, EditActivity.class);
        startActivityForResult(intent, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (intent == null) {
            return;
        }
        String command = intent.getStringExtra("Command");
        int id = intent.getIntExtra("Id", -1);
        String title = intent.getStringExtra("Title");
        String description = intent.getStringExtra("Description");
        try {
            DataManipulator.getInstance().executeCommand(command, id, title, description);
            Log.v("COMMAND", command + " " + id + " " + title + " " + description);
        } catch (InvalidCommandException e) {
            err.print(e.getStackTrace());
            return;
        }
        if (requestCode == 1) {
            adapter.notifyItemInserted(DataManipulator.getInstance().getSize() - 1);
        }
        if (requestCode == 2) {
            adapter.notifyDataSetChanged();
        }

    }

    private void SetData() {
        if (dbg_firsttime != 0) {
            try {
                DataManipulator.getInstance().executeCommand("UPDATE", -1, "p1", null);
                DataManipulator.getInstance().executeCommand("UPDATE", -1, "p2", "text2");
                DataManipulator.getInstance().executeCommand("UPDATE", -1, "p3", "text3");
            } catch (InvalidCommandException e) {
                err.println("Something fucked up in initial data creation");
            }
        }
    }


}
