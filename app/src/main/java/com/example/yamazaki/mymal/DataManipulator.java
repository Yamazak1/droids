package com.example.yamazaki.mymal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Yamazaki on 03.02.2017.
 */
class InvalidCommandException extends Exception {
    public InvalidCommandException(String s) {
        super(s);
    }
}

public class DataManipulator {
    private ArrayList<ListItem> data;
    private static volatile DataManipulator instance;
    private final String[] COMMANDS = {"DELETE", "UPDATE"};

    private DataManipulator() {
        this.data = new ArrayList<>();
    }

    public int getSize() {
        return data.size();
    }

    public ListItem getItem(int position) {
        return data.get(position);
    }

    // What the fuck I just did? Thanks, Habrahabr
    public static DataManipulator getInstance() {
        DataManipulator localInstance = instance;
        if (localInstance == null) {
            synchronized (DataManipulator.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataManipulator();
                }
            }

        }
        return localInstance;
    }

    private void updateItem(int id, String title, String description) {
        if (id == -1 || data.isEmpty()) {
            data.add(new ListItem(title, description));
            return;
        }
        for (int i = 0; i < data.size(); i++) {
            ListItem s = data.get(i);
            if (s.getId() == id) {
                s.setTitle(title);
                s.setDescription(description);
                return;
            }
        }
        data.add(new ListItem(title, description));
    }

    private void deleteItem(int id) {
        if (id == -1) {
            return;
        }
        for (int i = 0; i < data.size(); i++) {
            ListItem s = data.get(i);
            if (s.getId() == id) {
                data.remove(s);
                return;
            }
        }
    }

    public void executeCommand(String command, int id, String title, String description) throws InvalidCommandException {
        if (!Arrays.asList(COMMANDS).contains(command)) {
            throw new InvalidCommandException("INVALID COMMAND");
        }
        if (command.equals("UPDATE")) {
            updateItem(id, title, description);
        }
        if (command.equals("DELETE")) {
            deleteItem(id);
        }


    }

    public void DropData() {
        data.clear();
    }

}
