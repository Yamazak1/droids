package com.example.yamazaki.mymal;

import android.view.View;

/**
 * Created by Yamazaki on 01.02.2017.
 */

public interface OnEditButtonClickListener {
    void onEditIsClick(View button, int position);
}
